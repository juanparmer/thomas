# -*- coding: utf-8 -*-
{
    'name': "Contacts User",

    'summary': "Centralize your address book",

    'description': """
        This module gives you a quick view of your contacts directory, accessible from your home page.
        You can track your vendors, customers and other contacts.
    """,

    'author': "Juan Arcos",
    'website': "https://gitlab.com/juanparmer/thomas",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/14.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Sales/CRM',
    'version': '14.1',

    # any module necessary for this one to work correctly
    'depends': ['contacts'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'views/res_partner_genre_view.xml',
        'views/res_partner_view.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/res_partner_genre_demo.xml',
        'demo/res_partner_demo.xml',
    ],
}
