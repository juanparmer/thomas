
from random import randint

from odoo import models, fields, api


class ResPartnerGenre(models.Model):
    _name = 'res.partner.genre'
    _description = 'Music Genre'
    _order = 'sequence'

    def _get_default_color(self):
        return randint(1, 11)

    def _default_company_id(self):
        return self.env.company

    active = fields.Boolean(
        string='Active',
        default=True,
        copy=False
    )
    color = fields.Integer(
        string='Color',
        default=_get_default_color
    )
    company_id = fields.Many2one(
        comodel_name='res.company',
        name='Company',
        required=True,
        default=_default_company_id
    )
    name = fields.Char(
        string='Name',
        required=True
    )
    sequence = fields.Integer(string='Sequence')
