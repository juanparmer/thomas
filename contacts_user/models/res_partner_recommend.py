
import requests
from random import randint

from odoo import models, fields, api

CLIENT_ID = 'e6bdbb6792f44ecc96ebaf9bfaa8e602'
CLIENT_SECRET = '2796d5948e2348b08ee542b8098351b5'

AUTH_URL = 'https://accounts.spotify.com/api/token'
BASE_URL = 'https://api.spotify.com/v1/'


class ResPartneRrecommend(models.Model):
    _name = 'res.partner.recommend'
    _description = 'Recommended song'

    def _default_company_id(self):
        return self.env.company

    album = fields.Char(
        string='Album',
    )
    artist = fields.Char(
        string='Artist',
    )
    company_id = fields.Many2one(
        comodel_name='res.company',
        name='Company',
        required=True,
        default=_default_company_id
    )
    name = fields.Char(
        string='Name',
        required=True
    )
    genre_id = fields.Many2one(
        comodel_name='res.partner.genre',
        string='Genre',
        required=True
    )
    partner_id = fields.Many2one(
        comodel_name='res.partner',
        name='Partner',
        required=True,
        ondelete='cascade'
    )
    url = fields.Char(
        string='URL',
        required=True
    )

    def spotify_web_api(self):
        auth_response = requests.post(AUTH_URL, {
            'grant_type': 'client_credentials',
            'client_id': CLIENT_ID,
            'client_secret': CLIENT_SECRET,
        })

        auth_response_data = auth_response.json()
        access_token = auth_response_data['access_token']

        headers = {
            'Authorization': 'Bearer {token}'.format(token=access_token)
        }

        genre = False
        if self._context.get('genre_id'):
            Genre = self.env['res.partner.genre']
            genre = Genre.browse(self._context.get('genre_id'))

        genre = genre and genre.name or 'rock'
        market = self._context.get('market') or 'CO'

        track_search = '?query=genre:%s&type=track&market=%s&limit=50' % (
            genre, market
        )
        result = requests.get(
            BASE_URL + 'search' + track_search,
            headers=headers
        )

        if result.status_code != 200:
            return False, False

        result = result.json()

        index = randint(0, 49)

        album = result['tracks']['items'][index]['album']['name']
        artist = result['tracks']['items'][index]['artists'][0]['name']
        name = result['tracks']['items'][index]['name']
        url = result['tracks']['items'][index]['external_urls']['spotify']

        values = {
            'album': album,
            'artist': artist,
            'name': name,
            'url': url
        }

        return values
