
from odoo import models, fields, api


class ResPartner(models.Model):
    _inherit = 'res.partner'

    genres_ids = fields.Many2many(
        comodel_name='res.partner.genre',
        name='Genres',
    )
    recommend_ids = fields.One2many(
        comodel_name='res.partner.recommend',
        inverse_name='partner_id',
        name='Recommends'
    )

    @api.model_create_multi
    def create(self, vals_list):
        partners = super(ResPartner, self).create(vals_list)
        partners.action_recommend()
        return partners

    def write(self, vals):
        res = super(ResPartner, self).write(vals)
        if 'genres_ids' in vals:
            self.action_recommend()
        return res

    def action_recommend(self):
        for partner in self:
            partner._action_recommend()

    def _action_recommend(self):
        self.ensure_one()
        # Check if uinlink some recommend
        self._unlink_recommend().unlink()
        # List to create recommend
        value_recommend = []
        for genre_id in self.genres_ids:
            # Check if create recommend
            if not self._create_recommend(genre_id):
                value_recommend.append(
                    (0, 0, self._values_recommend(genre_id)))
        if value_recommend:
            self.write({'recommend_ids': value_recommend})
        return True

    def _unlink_recommend(self):
        self.ensure_one()
        return self.recommend_ids.filtered(lambda r: r.genre_id.id not in self.genres_ids.ids)

    def _create_recommend(self, genre_id):
        self.ensure_one()
        return self.recommend_ids.filtered(lambda r: r.genre_id.id == genre_id.id)

    def _values_recommend(self, genre_id):
        self.ensure_one()
        Recommend = self.env['res.partner.recommend']
        Recommend = Recommend.with_context(
            genre_id=genre_id.id,
            market=self.country_id and self.country_id.code or 'CO'
        )
        values_spotify = Recommend.spotify_web_api()
        values = {
            'album': values_spotify.get('album'),
            'artist': values_spotify.get('artist'),
            'genre_id': genre_id.id,
            'name': values_spotify.get('name'),
            'url': values_spotify.get('url'),
        }
        return values
