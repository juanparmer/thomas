
from odoo import _, models, fields, api

line_default = [
    {
        'name': 'Zapatos XYZ',
        'code': '8569741233658',
        'maker': 'Deportes XYZ',
        'category': 'Zapatos',
        'gender': 'Masculino',
    },
    {
        'name': 'Zapatos ABC',
        'code': '7452136985471',
        'maker': 'Deportes XYZ',
        'category': 'Zapatos',
        'gender': 'Femenino',
    },
    {
        'name': 'Camisa DEF',
        'code': '5236412896324',
        'maker': 'Deportes XYZ',
        'category': 'Camisas',
        'gender': 'Masculino',
    },
    {
        'name': 'Bolso KLM',
        'code': '5863219635478',
        'maker': 'Carteras Hi-Fashion',
        'category': 'Bolsos',
        'gender': 'Femenino',
    },
]


class KnowledgeExerciseFour(models.TransientModel):
    _name = 'knowledge.exercise.four'
    _inherit = 'knowledge.exercise'
    _description = 'Knowledge Exercise Four'

    def _default_line_ids(self):
        line_ids = []
        for line in line_default:
            line_ids.append((0, 0, line))

        return line_ids

    line_ids = fields.One2many(
        comodel_name='knowledge.exercise.four.line',
        inverse_name='four_id',
        default=_default_line_ids,
    )

    def action_calculate(self):
        res = super(KnowledgeExerciseFour, self).action_calculate()

        by_maker = [line.maker for line in self.line_ids]
        by_maker = list(dict.fromkeys(by_maker))
        by_category = [line.category for line in self.line_ids]
        by_category = list(dict.fromkeys(by_category))
        by_gender = [line.gender for line in self.line_ids]
        by_gender = list(dict.fromkeys(by_gender))

        values = {}
        for maker in by_maker:
            values_maker = {}
            for category in by_category:
                values_category = {}
                for gender in by_gender:
                    lines = self.line_ids.filtered(
                        lambda l: l.maker == maker and l.category == category and l.gender == gender
                    )
                    values_gender = {}
                    for line in lines:
                        values_gender['name'] = line.name
                    if not values_gender:
                        continue
                    values_category[gender] = values_gender
                if not values_category:
                    continue
                values_maker[category] = values_category
            if not values_maker:
                continue
            values[maker] = values_maker

        result = values
        self.write({'result': result})

        return res

    def action_done(self):
        result = super(KnowledgeExerciseFour, self).action_done()
        if self._context.get('exercise_four'):
            self.action_calculate()

            Window = self.env['ir.actions.act_window']
            action_name = 'knowledge_user.action_knowledge_exercise_four'
            action = Window._for_xml_id(action_name)
            action['res_id'] = self.id

            return action
        else:
            result


class KnowledgeExerciseFourLine(models.TransientModel):
    _name = 'knowledge.exercise.four.line'
    _description = 'Knowledge Exercise Four Line'

    category = fields.Char(
        string='Category'
    )
    code = fields.Char(
        string='Code',
    )
    four_id = fields.Many2one(
        comodel_name='knowledge.exercise.four',
        string='Four',
        required=True,
        ondelete='cascade',
    )
    gender = fields.Char(
        string='Gender',
    )
    maker = fields.Char(
        string='Maker'
    )
    name = fields.Char(
        string='Name',
    )
