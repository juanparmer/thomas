
from odoo import _, models, fields, api
from odoo.exceptions import ValidationError


class KnowledgeExerciseOne(models.TransientModel):
    _name = 'knowledge.exercise.one'
    _inherit = 'knowledge.exercise'
    _description = 'Knowledge Exercise One'

    def _default_one_text(self):
        return '4 abc abcdefg abc bcdefg'

    one_bool = fields.Boolean(
        string='Default?',
        default=True,
        help='Select to use default value',
    )
    one_text = fields.Text(
        string='Text',
        default=_default_one_text,
    )

    @api.onchange('one_bool')
    def _onchange_one_bool(self):
        one_text = self.one_bool and self._default_one_text() or False
        self.update({'one_text': one_text})

    def action_calculate(self):
        res = super(KnowledgeExerciseOne, self).action_calculate()

        split_text = self.one_text.split(' ')

        try:
            int(split_text[0])
        except:
            raise ValidationError(_('Bad text format'))

        unique_text = list(dict.fromkeys(split_text[1:]))

        res_text = ''
        for text in unique_text:
            len_text = len([t for t in split_text if text == t])
            res_text += '%s ' % str(len_text)
        
        result = '%s\n%s' % (len(unique_text), res_text)

        self.write({'result': result})
        return res

    def action_done(self):
        result = super(KnowledgeExerciseOne, self).action_done()
        if self._context.get('exercise_one'):
            self.action_calculate()

            Window = self.env['ir.actions.act_window']
            action_name = 'knowledge_user.action_knowledge_exercise_one'
            action = Window._for_xml_id(action_name)
            action['res_id'] = self.id

            return action
        else:
            result
