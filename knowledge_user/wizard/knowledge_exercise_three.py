
from datetime import datetime, timedelta
from pytz import timezone

from odoo import _, models, fields, api
from odoo.exceptions import ValidationError
from odoo.addons.base.models.res_partner import _tz_get


class KnowledgeExerciseThree(models.TransientModel):
    _name = 'knowledge.exercise.three'
    _inherit = 'knowledge.exercise'
    _description = 'Knowledge Exercise Three'

    def _default_three_tz(self):
        return self.env.user.tz

    three_datetime_one = fields.Datetime(
        string='Date A',
        default=fields.Datetime.now(),
    )
    three_tz_one = fields.Selection(
        selection=_tz_get,
        string='Timezone A',
        default=_default_three_tz
    )
    three_datetime_two = fields.Datetime(
        string='Date B',
        default=fields.Datetime.now(),
    )
    three_tz_two = fields.Selection(
        selection=_tz_get,
        string='Timezone B',
        default=_default_three_tz
    )

    def action_calculate(self):
        res = super(KnowledgeExerciseThree, self).action_calculate()

        datetime_one = datetime(
            self.three_datetime_one.year,
            self.three_datetime_one.month,
            self.three_datetime_one.day,
            self.three_datetime_one.hour,
            self.three_datetime_one.minute,
            self.three_datetime_one.second,
            self.three_datetime_one.microsecond,
            timezone(self.three_tz_one),
        )

        datetime_two = datetime(
            self.three_datetime_two.year,
            self.three_datetime_two.month,
            self.three_datetime_two.day,
            self.three_datetime_two.hour,
            self.three_datetime_two.minute,
            self.three_datetime_two.second,
            self.three_datetime_two.microsecond,
            timezone(self.three_tz_two),
        )

        if datetime_one > datetime_two:
            raise ValidationError(_('Bad date format'))

        result = self.action_calculate_days(datetime_one, datetime_two)
        self.write({'result': result})
        return res

    def action_calculate_days(self, date_one, date_two):
        date = date_two - date_one

        days = []
        for day in range(date.days):
            days.append(date_one + timedelta(days=day))

        result = _('''
        Days number:
        Monday: %s
        Tuesday: %s
        Wednesday: %s
        Thursday: %s
        Friday: %s
        Saturday: %s
        Sunday: %s

        Difference between dates:
        Seconds: %s
        Hours: %s
        Days: %s
        ''') % (
            len([d for d in days if d.weekday() == 0]),
            len([d for d in days if d.weekday() == 1]),
            len([d for d in days if d.weekday() == 2]),
            len([d for d in days if d.weekday() == 3]),
            len([d for d in days if d.weekday() == 4]),
            len([d for d in days if d.weekday() == 5]),
            len([d for d in days if d.weekday() == 6]),
            int(date.total_seconds()),
            int(date.total_seconds()/3600),
            date.days,
        )
        return result

    def action_done(self):
        result = super(KnowledgeExerciseThree, self).action_done()
        if self._context.get('exercise_three'):
            self.action_calculate()

            Window = self.env['ir.actions.act_window']
            action_name = 'knowledge_user.action_knowledge_exercise_three'
            action = Window._for_xml_id(action_name)
            action['res_id'] = self.id

            return action
        else:
            result
