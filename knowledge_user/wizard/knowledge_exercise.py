
from odoo import models, fields, api


class KnowledgeExercise(models.TransientModel):
    _name = 'knowledge.exercise'
    _description = 'Knowledge Exercise'

    name = fields.Char(string='name')
    result = fields.Text(
        string='Result',
    )
    state = fields.Selection(
        selection=[
            ('draft', 'Draft'),
            ('done', 'Done'),
        ],
        name='State',
        default='draft'
    )

    def action_done(self):
        return self.write({'state': 'done'})

    def action_calculate(self):
        return True

    def action_exercise(self):
        Window = self.env['ir.actions.act_window']
        exercise_name = self._context.get('exercise_name')
        action_name = 'knowledge_user.action_knowledge_%s' % exercise_name
        action = Window._for_xml_id(action_name)
        return action

    def action_exercises(self):
        Window = self.env['ir.actions.act_window']
        action_name = 'knowledge_user.action_knowledge_exercise'
        action = Window._for_xml_id(action_name)
        return action
