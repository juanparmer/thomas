
from odoo import _, models, fields, api


class KnowledgeExerciseTwo(models.TransientModel):
    _name = 'knowledge.exercise.two'
    _inherit = 'knowledge.exercise'
    _description = 'Knowledge Exercise Two'

    two_complex_ax = fields.Integer(
        string='AX',
        default=2,
    )
    two_complex_ay = fields.Integer(
        string='AY',
        default=1,
    )
    two_complex_bx = fields.Integer(
        string='BX',
        default=5,
    )
    two_complex_by = fields.Integer(
        string='BY',
        default=6,
    )

    def action_calculate(self):
        res = super(KnowledgeExerciseTwo, self).action_calculate()

        cax = self.two_complex_ax
        cay = self.two_complex_ay
        ca = complex(cax, cay)

        cbx = self.two_complex_bx
        cby = self.two_complex_by
        cb = complex(cbx, cby)

        values = self.action_complex(ca, cb)
        result = '''
        A + B → %s
        A - B → %s
        A * B → %s
        A / B → %s
        Mod(A) → %s
        Mod(B) → %s
        ''' % (
            values.get('apb'),
            values.get('asb'),
            values.get('amb'),
            values.get('adb'),
            values.get('ma'),
            values.get('mb'),
        )

        self.write({'result': result})
        return res

    def action_complex(self, ca, cb):
        values = {
            'apb': ca + cb,
            'asb': ca - cb,
            'amb': ca * cb,
            'adb': ca / cb,
            'ma': abs(ca),
            'mb': abs(cb),
        }
        return values

    def action_done(self):
        result = super(KnowledgeExerciseTwo, self).action_done()
        if self._context.get('exercise_two'):
            self.action_calculate()

            Window = self.env['ir.actions.act_window']
            action_name = 'knowledge_user.action_knowledge_exercise_two'
            action = Window._for_xml_id(action_name)
            action['res_id'] = self.id

            return action
        else:
            result
