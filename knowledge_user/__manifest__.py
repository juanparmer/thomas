# -*- coding: utf-8 -*-
{
    'name': "Knowledge Exercise",

    'summary': "Knowledge Exercise Thomas",

    'description': "Knowledge Exercise",

    'author': "Juan Arcos",
    'website': "https://gitlab.com/juanparmer/thomas",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/14.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Knowledge',
    'version': '14.1',

    # any module necessary for this one to work correctly
    'depends': ['resource'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        # 'views/views.xml',
        # 'views/templates.xml',
        'wizard/knowledge_exercise_view.xml',
        'wizard/knowledge_exercise_one_view.xml',
        'wizard/knowledge_exercise_two_view.xml',
        'wizard/knowledge_exercise_three_view.xml',
        'wizard/knowledge_exercise_four_view.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}
